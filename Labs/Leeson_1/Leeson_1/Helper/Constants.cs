﻿using System;


namespace Leeson_1.Helper
{
    class Constants
    {
        public const string DEBUG = "DEBUG";
        public const string INFO = "INFO";
        public const string WARN = "WARN";
        public const string ERROR = "ERROR";
        public const string FATAL = "FATAL";
        public const string LOG_OUTPUT_FILE = "Logger.log";
        public const string INPUT_SUM_HEAD_INFO = "Введите любое дробное число!";
        public const string INPUT_CONVERT_HED_INFO = "Преобразование числа в денежный формат!";
        public const string BUTTON_OK_TXT = "OK";
        public const string BUTTON_EXIT_TXT = "EXIT";
        public const string INPUT_TASK_HED_INFO = "Вычисление стоимости поездки на дачу!";
        public const string LABEL_TASK_INFO_S = "Расстояние до дачи(км) – ";
        public const string LABEL_TASK_INFO_100_KM = "Расход бензина(л на 100 км) – ";
        public const string LABEL_TASK_INFO_RUB = "Цена литра бензина(руб.) – ";
        public const string LABEL_TASK_INFO_RESULT = "Поездка на дачу обойдется в {0}";

        public const string INPUT_FUNC_HEAD_INFO = "ax2 + bx + c = 0 и mx + n = 0 относительно х";
        public const string LABEL_FUNC_INFO_RESULT = "bool";
        public const string LABEL_FUNC_A = "a - ";
        public const string LABEL_FUNC_B = "b - ";
        public const string LABEL_FUNC_M = "m - ";
        public const string LABEL_FUNC_N = "n - ";
        public const string LABEL_FUNC_C = "c - ";

    }
}
