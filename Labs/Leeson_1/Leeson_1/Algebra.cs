﻿using Leeson_1.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Leeson_1
{
    class Algebra
    {

        public int calculationSum(double number) 
        {
            Log.DEBUG("Entered number was ====>> {0}", number);

            double doub = number - (int)number;
            int k = 0;
            Log.DEBUG("Entered number was ====>> {0}", doub.ToString());
            int sum = 0;
            
            for (int i = 2; i <= 4; i++)
            {
                doub = doub * 10;
                Log.DEBUG("[{0}]->number   doub==> {1}", i, doub);
                k = Convert.ToInt32(doub.ToString()[0].ToString());
                sum = sum + k;
                Log.DEBUG("[{0}]->number==> {1}", i, k);
                doub = doub - (int)doub;

            }
            return sum;
        }
        
        public string calculationTask(int num1, double num2, double num3) 
        {
            Log.DEBUG("Entered calculationTask number was ====>> {0}, {1}, {2}", num1, num2, num3);

            double doub = (((num2/100)*num1)*num3)*2;
            Log.DEBUG("Result number==> {0}", doub);
            return ConverterMoney(doub);
        }

        public string ConverterMoney(double number)
        {
            Log.DEBUG("Entered number was ====>> {0}", number);

            double doub = number - (int)number;
            string str = string.Format("{0:N2}", doub);
            int sum = 0;

            for (int i = 2; i < str.Length; i++)
            {
                int k = Convert.ToInt32(str[i].ToString());
                sum = sum + k;
                Log.DEBUG("[{0}]->number==> {1}", i, k);
            }
            return string.Format("{0} руб. {1}{2} коп.", (int)number, str[2], str[3]);
        }

        public bool calculationFunc(int a, int b, int c, int m, int n)
        {
            Log.DEBUG("Entered calculationFunc number was ====>> {0}, {1}, {2}, {3}, {4}", a, b, c, m, n);

            if ((n / m == 0) | ((b * b) - (4 * a * c) >= 0))
            {
                return true;
            }
            return false;
        }
    }
}
