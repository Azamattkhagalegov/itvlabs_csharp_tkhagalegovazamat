﻿namespace Leeson_1.Window
{
    class MainTabPage
    {

        private static Algebra _Algebra = new Algebra();
        private static Parameters _Parameters = new Parameters();
        public static void CloseWindow(object sender, System.EventArgs e)
        {
            System.Windows.Forms.Application.Exit();
        }

        public static void EnterOnlyDigits(object sender, System.Windows.Forms.KeyPressEventArgs e)
        {
            char c = e.KeyChar;
            e.Handled = !(char.IsDigit(c)
                          || c == '.'
                          || c == ','
                          || c == 8);// we can enter number and dot
        }

        public static void EnterOnlyDigitsWithoutChar(object sender, System.Windows.Forms.KeyPressEventArgs e)
        {
            char c = e.KeyChar;
            e.Handled = !(char.IsDigit(c)
                          || c == 8
                          || c == 45);// we can enter number
        }


        public class SumTabPage : System.Windows.Forms.TabPage
        {
            public void SumThreeNumber(object sender, System.EventArgs e)
            {
                _Parameters._Number = _Algebra.calculationSum(System.Convert.ToDouble(_InputTextBox.Text));
                _outPutResultInfo.Text = _Parameters._Number.ToString();
            }
            public void Initialized()
            {
                _buttonOK = new System.Windows.Forms.Button();
                _buttonExit = new System.Windows.Forms.Button();
                _InputTextBox = new System.Windows.Forms.TextBox();
                _outPutResultInfo = new System.Windows.Forms.Label();
                _headInfoLabel = new System.Windows.Forms.Label();
                //Button OK
                _buttonOK.Text = Helper.Constants.BUTTON_OK_TXT;
                _buttonOK.Size = new System.Drawing.Size(100, 50);
                _buttonOK.Location = new System.Drawing.Point(100, 400);
                _buttonOK.BackColor = System.Drawing.Color.White;
                _buttonOK.Font = new System.Drawing.Font("Aray", 14);
                _buttonOK.Click += SumThreeNumber;

                //Button Exit
                _buttonExit.Text = Helper.Constants.BUTTON_EXIT_TXT;
                _buttonExit.Size = new System.Drawing.Size(100, 50);
                _buttonExit.Location = new System.Drawing.Point(350, 400);
                _buttonExit.BackColor = System.Drawing.Color.White;
                _buttonExit.Font = new System.Drawing.Font("Aray", 14);
                _buttonExit.Click += CloseWindow;

                // _InputTextBox
                _InputTextBox.Size = new System.Drawing.Size(400, 100);
                _InputTextBox.Location = new System.Drawing.Point(100, 100);
                _InputTextBox.Font = new System.Drawing.Font("Aray", 32);
                _InputTextBox.KeyPress += EnterOnlyDigits;
                // _outPutResultInfo
                _outPutResultInfo.Text = "C#";
                _outPutResultInfo.Size = new System.Drawing.Size(400, 300);
                _outPutResultInfo.Location = new System.Drawing.Point(100, 200);
                _outPutResultInfo.Font = new System.Drawing.Font("Georgia", 72);
                //_headInfoLabel
                _headInfoLabel.Text = Helper.Constants.INPUT_SUM_HEAD_INFO;
                _headInfoLabel.Size = new System.Drawing.Size(400, 300);
                _headInfoLabel.Location = new System.Drawing.Point(100, 50);
                _headInfoLabel.Font = new System.Drawing.Font("Georgia", 18);
                ///

                this.BackColor = System.Drawing.Color.DarkRed;
                this.Text = "Sum";
                this.Font = new System.Drawing.Font("Aray", 11);
                this.Controls.Add(_buttonOK);
                this.Controls.Add(_buttonExit);
                this.Controls.Add(_InputTextBox);
                this.Controls.Add(_outPutResultInfo);
                this.Controls.Add(_headInfoLabel);
            }
            private static System.Windows.Forms.Button _buttonOK;
            private static System.Windows.Forms.Button _buttonExit;
            private static System.Windows.Forms.TextBox _InputTextBox;
            private static System.Windows.Forms.Label _outPutResultInfo;
            public static System.Windows.Forms.Label _headInfoLabel;

        }

        public class ConvertTabPage : System.Windows.Forms.TabPage
        {
            public void Convert(object sender, System.EventArgs e)
            {

                _outPutResultInfo.Text = _Algebra.ConverterMoney(System.Convert.ToDouble(_InputTextBox.Text));
            }

            public void Initialized()
            {
                _buttonOK = new System.Windows.Forms.Button();
                _buttonExit = new System.Windows.Forms.Button();
                _InputTextBox = new System.Windows.Forms.TextBox();
                _outPutResultInfo = new System.Windows.Forms.Label();
                _headInfoLabel = new System.Windows.Forms.Label();
                //Button OK
                _buttonOK.Text = Helper.Constants.BUTTON_OK_TXT;
                _buttonOK.Size = new System.Drawing.Size(100, 50);
                _buttonOK.Location = new System.Drawing.Point(100, 400);
                _buttonOK.BackColor = System.Drawing.Color.White;
                _buttonOK.Font = new System.Drawing.Font("Aray", 14);
                _buttonOK.Click += Convert;

                //Button Exit
                _buttonExit.Text = Helper.Constants.BUTTON_EXIT_TXT;
                _buttonExit.Size = new System.Drawing.Size(100, 50);
                _buttonExit.Location = new System.Drawing.Point(350, 400);
                _buttonExit.BackColor = System.Drawing.Color.White;
                _buttonExit.Font = new System.Drawing.Font("Aray", 14);
                _buttonExit.Click += CloseWindow;

                // _InputTextBox
                _InputTextBox.Size = new System.Drawing.Size(400, 100);
                _InputTextBox.Location = new System.Drawing.Point(100, 120);
                _InputTextBox.Font = new System.Drawing.Font("Aray", 32);
                _InputTextBox.KeyPress += EnterOnlyDigits;
                // _outPutResultInfo
                _outPutResultInfo.Text = "C#";
                _outPutResultInfo.Size = new System.Drawing.Size(400, 300);
                _outPutResultInfo.Location = new System.Drawing.Point(100, 200);
                _outPutResultInfo.Font = new System.Drawing.Font("Georgia", 32);
                //_headInfoLabel
                _headInfoLabel.Text = Helper.Constants.INPUT_CONVERT_HED_INFO;
                _headInfoLabel.Size = new System.Drawing.Size(400, 300);
                _headInfoLabel.Location = new System.Drawing.Point(100, 50);
                _headInfoLabel.Font = new System.Drawing.Font("Georgia", 18);
                //
                this.BackColor = System.Drawing.Color.DarkGray;
                this.Text = "Convert";
                this.Font = new System.Drawing.Font("Aray", 11);
                this.Controls.Add(_buttonOK);
                this.Controls.Add(_buttonExit);
                this.Controls.Add(_InputTextBox);
                this.Controls.Add(_outPutResultInfo);
                this.Controls.Add(_headInfoLabel);
            }
            private static System.Windows.Forms.Button _buttonOK;
            private static System.Windows.Forms.Button _buttonExit;
            private static System.Windows.Forms.TextBox _InputTextBox;
            private static System.Windows.Forms.Label _outPutResultInfo;
            public static System.Windows.Forms.Label _headInfoLabel;
        }

        public class TaskTabPage : System.Windows.Forms.TabPage
        {
            public void CalculationTask(object sender, System.EventArgs e)
            {
                int number1 = System.Convert.ToInt32(_InputFistTextBox.Text);
                double number2 = System.Convert.ToDouble(_Input100KmTextBox.Text);
                double number3 = System.Convert.ToDouble(_InputRubTextBox.Text);

                string result = _Algebra.calculationTask(number1, number2, number3);
                _outPutResultInfo.Text = string.Format(Helper.Constants.LABEL_TASK_INFO_RESULT, result);
            }

            public void Initialized()
            {

                _buttonOK = new System.Windows.Forms.Button();
                _buttonExit = new System.Windows.Forms.Button();
                _InputFistTextBox = new System.Windows.Forms.TextBox();
                _Input100KmTextBox = new System.Windows.Forms.TextBox();
                _InputRubTextBox = new System.Windows.Forms.TextBox();
                _outPutResultInfo = new System.Windows.Forms.Label();
                _headInfoLabel = new System.Windows.Forms.Label();
                _infoSLabel = new System.Windows.Forms.Label();
                _info100KmLabel = new System.Windows.Forms.Label();
                _infoRubLabel = new System.Windows.Forms.Label();
                //Button OK
                _buttonOK.Text = Helper.Constants.BUTTON_OK_TXT;
                _buttonOK.Size = new System.Drawing.Size(100, 50);
                _buttonOK.Location = new System.Drawing.Point(100, 400);
                _buttonOK.BackColor = System.Drawing.Color.White;
                _buttonOK.Font = new System.Drawing.Font("Aray", 14);
                _buttonOK.Click += CalculationTask;

                //Button Exit
                _buttonExit.Text = Helper.Constants.BUTTON_EXIT_TXT;
                _buttonExit.Size = new System.Drawing.Size(100, 50);
                _buttonExit.Location = new System.Drawing.Point(350, 400);
                _buttonExit.BackColor = System.Drawing.Color.White;
                _buttonExit.Font = new System.Drawing.Font("Aray", 14);
                _buttonExit.Click += CloseWindow;

                // _InputFistTextBox
                _InputFistTextBox.Size = new System.Drawing.Size(50, 50);
                _InputFistTextBox.Location = new System.Drawing.Point(400, 150);
                _InputFistTextBox.Font = new System.Drawing.Font("Aray", 11);
                _InputFistTextBox.KeyPress += EnterOnlyDigits;

                // _Input100KmTextBox
                _Input100KmTextBox.Size = new System.Drawing.Size(50, 50);
                _Input100KmTextBox.Location = new System.Drawing.Point(400, 200);
                _Input100KmTextBox.Font = new System.Drawing.Font("Aray", 11);
                _Input100KmTextBox.KeyPress += EnterOnlyDigits;

                // _InputRubTextBox
                _InputRubTextBox.Size = new System.Drawing.Size(50, 50);
                _InputRubTextBox.Location = new System.Drawing.Point(400, 250);
                _InputRubTextBox.Font = new System.Drawing.Font("Aray", 11);
                _InputRubTextBox.KeyPress += EnterOnlyDigits;

                // _outPutResultInfo
                _outPutResultInfo.Text = string.Format(Helper.Constants.LABEL_TASK_INFO_RESULT, 0, 00);
                _outPutResultInfo.Size = new System.Drawing.Size(400, 50);
                _outPutResultInfo.Location = new System.Drawing.Point(50, 300);
                _outPutResultInfo.Font = new System.Drawing.Font("Georgia", 12);
                //_infoSLabel
                _infoSLabel.Text = Helper.Constants.LABEL_TASK_INFO_S;
                _infoSLabel.Size = new System.Drawing.Size(400, 50);
                _infoSLabel.Location = new System.Drawing.Point(50, 150);
                _infoSLabel.Font = new System.Drawing.Font("Georgia", 12);

                //_info100KmLabel
                _info100KmLabel.Text = Helper.Constants.LABEL_TASK_INFO_100_KM;
                _info100KmLabel.Size = new System.Drawing.Size(400, 50);
                _info100KmLabel.Location = new System.Drawing.Point(50, 200);
                _info100KmLabel.Font = new System.Drawing.Font("Georgia", 12);
                
                //_headInfoLabel
                _headInfoLabel.Text = Helper.Constants.INPUT_TASK_HED_INFO;
                _headInfoLabel.Size = new System.Drawing.Size(400, 50);
                _headInfoLabel.Location = new System.Drawing.Point(100, 50);
                _headInfoLabel.Font = new System.Drawing.Font("Georgia", 18);

                //_infoRubLabel
                _infoRubLabel.Text = Helper.Constants.LABEL_TASK_INFO_RUB;
                _infoRubLabel.Size = new System.Drawing.Size(400, 50);
                _infoRubLabel.Location = new System.Drawing.Point(50, 250);
                _infoRubLabel.Font = new System.Drawing.Font("Georgia", 12);
                ///
                this.BackColor = System.Drawing.Color.DarkViolet;
                this.Text = "Task";
                this.Font = new System.Drawing.Font("Aray", 11);
                this.Controls.Add(_buttonOK);
                this.Controls.Add(_buttonExit);
                this.Controls.Add(_InputFistTextBox);
                this.Controls.Add(_Input100KmTextBox);
                this.Controls.Add(_InputRubTextBox);
                this.Controls.Add(_infoSLabel);
                this.Controls.Add(_outPutResultInfo);
                this.Controls.Add(_headInfoLabel);
                this.Controls.Add(_info100KmLabel);
                this.Controls.Add(_infoRubLabel);
                

            }
            private static System.Windows.Forms.Button _buttonOK;
            private static System.Windows.Forms.Button _buttonExit;
            private static System.Windows.Forms.TextBox _InputFistTextBox;
            private static System.Windows.Forms.TextBox _Input100KmTextBox;
            private static System.Windows.Forms.TextBox _InputRubTextBox;
            private static System.Windows.Forms.Label _outPutResultInfo;
            private static System.Windows.Forms.Label _headInfoLabel;
            private static System.Windows.Forms.Label _infoSLabel;
            private static System.Windows.Forms.Label _info100KmLabel;
            private static System.Windows.Forms.Label _infoRubLabel;

        }
        public class FuncTabPage : System.Windows.Forms.TabPage
        {
            public void SqrtFunc(object sender, System.EventArgs e)
            {
                int a = System.Convert.ToInt32(_InputTextBoxA.Text);
                int b = System.Convert.ToInt32(_InputTextBoxB.Text);
                int c = System.Convert.ToInt32(_InputTextBoxC.Text);
                int m = System.Convert.ToInt32(_InputTextBoxM.Text);
                int n = System.Convert.ToInt32(_InputTextBoxN.Text);

                bool isSqrt = _Algebra.calculationFunc(a, b, c, m, n);
                _outPutResultInfo.Text = isSqrt.ToString();
            }

            public void Initialized()
            {

                _buttonOK = new System.Windows.Forms.Button();
                _buttonExit = new System.Windows.Forms.Button();
                _InputTextBoxA = new System.Windows.Forms.TextBox();
                _InputTextBoxB = new System.Windows.Forms.TextBox();
                _InputTextBoxC = new System.Windows.Forms.TextBox();
                _InputTextBoxM = new System.Windows.Forms.TextBox();
                _InputTextBoxN = new System.Windows.Forms.TextBox();

                _outPutResultInfo = new System.Windows.Forms.Label();
                _headInfoLabel = new System.Windows.Forms.Label();
                _labelA = new System.Windows.Forms.Label();
                _labelB = new System.Windows.Forms.Label();
                _labelC = new System.Windows.Forms.Label();
                _labelM = new System.Windows.Forms.Label();
                _labelN = new System.Windows.Forms.Label();
                //Button OK
                _buttonOK.Text = Helper.Constants.BUTTON_OK_TXT;
                _buttonOK.Location = new System.Drawing.Point(100, 480);
                _buttonOK.BackColor = System.Drawing.Color.White;
                _buttonOK.Font = new System.Drawing.Font("Aray", 12);
                _buttonOK.Click += SqrtFunc;

                //Button Exit
                _buttonExit.Text = Helper.Constants.BUTTON_EXIT_TXT;
                _buttonExit.Location = new System.Drawing.Point(350, 480);
                _buttonExit.BackColor = System.Drawing.Color.White;
                _buttonExit.Font = new System.Drawing.Font("Aray", 12);
                _buttonExit.Click += CloseWindow;
                // _InputTextBoxA
                _InputTextBoxA.Size = new System.Drawing.Size(100, 50);
                _InputTextBoxA.Location = new System.Drawing.Point(250, 100);
                _InputTextBoxA.Font = new System.Drawing.Font("Aray", 14);
                _InputTextBoxA.KeyPress += EnterOnlyDigitsWithoutChar;
                // _InputTextBoxB
                _InputTextBoxB.Size = new System.Drawing.Size(100, 50);
                _InputTextBoxB.Location = new System.Drawing.Point(250, 150);
                _InputTextBoxB.Font = new System.Drawing.Font("Aray", 14);
                _InputTextBoxB.KeyPress += EnterOnlyDigitsWithoutChar;
                // _InputTextBoxC
                _InputTextBoxC.Size = new System.Drawing.Size(100, 50);
                _InputTextBoxC.Location = new System.Drawing.Point(250, 200);
                _InputTextBoxC.Font = new System.Drawing.Font("Aray", 14);
                _InputTextBoxC.KeyPress += EnterOnlyDigitsWithoutChar;
                // _InputTextBoxM
                _InputTextBoxM.Size = new System.Drawing.Size(100, 50);
                _InputTextBoxM.Location = new System.Drawing.Point(250, 250);
                _InputTextBoxM.Font = new System.Drawing.Font("Aray", 14);
                _InputTextBoxM.KeyPress += EnterOnlyDigitsWithoutChar;
                // _InputTextBoxN
                _InputTextBoxN.Size = new System.Drawing.Size(100, 50);
                _InputTextBoxN.Location = new System.Drawing.Point(250, 300);
                _InputTextBoxN.Font = new System.Drawing.Font("Aray", 14);
                _InputTextBoxN.KeyPress += EnterOnlyDigitsWithoutChar;

                // _outPutResultInfo
                _outPutResultInfo.Text = Helper.Constants.LABEL_FUNC_INFO_RESULT;
                _outPutResultInfo.Size = new System.Drawing.Size(400, 50);
                _outPutResultInfo.Location = new System.Drawing.Point(250, 350);
                _outPutResultInfo.Font = new System.Drawing.Font("Georgia", 18);

                // _headInfoLabel
                _headInfoLabel.Text = Helper.Constants.INPUT_FUNC_HEAD_INFO;
                _headInfoLabel.Size = new System.Drawing.Size(400, 50);
                _headInfoLabel.Location = new System.Drawing.Point(100, 30);
                _headInfoLabel.Font = new System.Drawing.Font("Georgia", 18);

                //_labelA
                _labelA.Text = Helper.Constants.LABEL_FUNC_A;
                _labelA.Size = new System.Drawing.Size(400, 50);
                _labelA.Location = new System.Drawing.Point(150, 100);
                _labelA.Font = new System.Drawing.Font("Georgia", 20);

                //_labelB
                _labelB.Text = Helper.Constants.LABEL_FUNC_B;
                _labelB.Size = new System.Drawing.Size(400, 50);
                _labelB.Location = new System.Drawing.Point(150, 150);
                _labelB.Font = new System.Drawing.Font("Georgia", 20);

                //_labelC
                _labelC.Text = Helper.Constants.LABEL_FUNC_C;
                _labelC.Size = new System.Drawing.Size(400, 50);
                _labelC.Location = new System.Drawing.Point(150, 200);
                _labelC.Font = new System.Drawing.Font("Georgia", 20);
                //_labelM
                _labelM.Text = Helper.Constants.LABEL_FUNC_M;
                _labelM.Size = new System.Drawing.Size(400, 50);
                _labelM.Location = new System.Drawing.Point(150, 250);
                _labelM.Font = new System.Drawing.Font("Georgia", 20);
                //_labelN
                _labelN.Text = Helper.Constants.LABEL_FUNC_N;
                _labelN.Size = new System.Drawing.Size(400, 50);
                _labelN.Location = new System.Drawing.Point(150, 300);
                _labelN.Font = new System.Drawing.Font("Georgia", 20);
                ///

                this.BackColor = System.Drawing.Color.DarkViolet;
                this.Text = "Function";
                this.Font = new System.Drawing.Font("Aray", 11);
                this.Controls.Add(_buttonOK);
                this.Controls.Add(_buttonExit);
                this.Controls.Add(_InputTextBoxA);
                this.Controls.Add(_InputTextBoxN);
                this.Controls.Add(_InputTextBoxB);
                this.Controls.Add(_InputTextBoxC);
                this.Controls.Add(_InputTextBoxM);
                
                this.Controls.Add(_outPutResultInfo);
                this.Controls.Add(_headInfoLabel);
                this.Controls.Add(_labelA);
                this.Controls.Add(_labelB);
                this.Controls.Add(_labelC);
                this.Controls.Add(_labelM);
                this.Controls.Add(_labelN);

            }

            private static System.Windows.Forms.Button _buttonOK;
            private static System.Windows.Forms.Button _buttonExit;

            private static System.Windows.Forms.TextBox _InputTextBoxA;
            private static System.Windows.Forms.TextBox _InputTextBoxB;
            private static System.Windows.Forms.TextBox _InputTextBoxC;
            private static System.Windows.Forms.TextBox _InputTextBoxM;
            private static System.Windows.Forms.TextBox _InputTextBoxN;

            private static System.Windows.Forms.Label _outPutResultInfo;
            private static System.Windows.Forms.Label _headInfoLabel;
            private static System.Windows.Forms.Label _labelA;
            private static System.Windows.Forms.Label _labelB;
            private static System.Windows.Forms.Label _labelC;
            private static System.Windows.Forms.Label _labelM;
            private static System.Windows.Forms.Label _labelN;

        }

    }
}
