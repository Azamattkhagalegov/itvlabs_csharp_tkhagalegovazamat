﻿using System;

namespace Leeson_1.Window
{
    class Parameters
    {
        private double _number;

        public double _Number
        {
            get { return _number; }
            set { _number = value; }
        }
    }
}
