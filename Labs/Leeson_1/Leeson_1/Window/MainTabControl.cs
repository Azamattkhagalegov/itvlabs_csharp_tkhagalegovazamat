﻿
using System.Windows.Forms;

namespace Leeson_1.Window
{
    class MainTabControl: TabControl
    {

        public MainTabPage.SumTabPage _sumTabPage = new MainTabPage.SumTabPage();
        private MainTabPage.ConvertTabPage _convertTabPage = new MainTabPage.ConvertTabPage();
        private MainTabPage.TaskTabPage _taskTabPage = new MainTabPage.TaskTabPage();
        private MainTabPage.FuncTabPage _funcTabPage = new MainTabPage.FuncTabPage();
        
        public void Initialized()
        {
            this.Size = new System.Drawing.Size(600, 600);

            _sumTabPage.Initialized();
            _convertTabPage.Initialized();
            _taskTabPage.Initialized();
            _funcTabPage.Initialized();
            this.Controls.Add(_sumTabPage);
            this.Controls.Add(_convertTabPage);
            this.Controls.Add(_taskTabPage);
            this.Controls.Add(_funcTabPage);


        }

    }
}
