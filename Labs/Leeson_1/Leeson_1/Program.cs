﻿using Leeson_1.Helper;
using System;
using System.Windows.Forms;
namespace Leeson_1
{
    class Program
    {
        static Window.MainWindow mainWindow = new Window.MainWindow();

        [STAThread]
        static void Main()
        {
            try
            {

                Log.INFO("==========Strat main program!!!==========");
                mainWindow.configurationWindow();
                mainWindow.ShowWindow();
            }

            catch (Exception ex)
            {
                Log.FATAL("{0}", ex);
                MessageBox.Show(string.Format("Not correct format number input {0}", ex), "Warrning", MessageBoxButtons.OK, MessageBoxIcon.Information);
                
            }
           
            Log.DEBUG("++++++The program was correct finished++++++{0}", "ok");
        }


    }
}
